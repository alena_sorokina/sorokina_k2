<?

define("IBLOCK_CAT_ID", 2);
define("IBLOCK_ACTIONS_ID", 5);
define("IBLOCK_NEWS_ID", 1);
define("ADMIN_GROUP_ID", 1);
define("GROUP_CONTENT_ID", 5);

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/functions-dump.php"))
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/functions-dump.php");

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/agent.php"))
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/agent.php");

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/event_handlers.php"))
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/event_handlers.php");
?>