<?
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("CIBLockHandler", "OnBeforeIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("CIBLockHandler", "OnBeforeIBlockElementDeleteHandler"));

class CIBLockHandler
{
    // пунткт 1
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID'] == IBLOCK_NEWS_ID)
        {
            $date_from = strtotime($arFields['ACTIVE_FROM']);
            $days3Ago = strtotime(ConvertTimeStamp(time()-(86400*3)));
            if ($arFields['ACTIVE'] == "N" && $date_from > $days3Ago){
                global $APPLICATION;
                $APPLICATION->ThrowException("Вы деактивировали свежую новость");
                return false;
            }
        }
    }

    //пункт 2
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        $iblock_id = CIBlockElement::GetIBlockByID($ID);
        $res = CIBlockElement::GetByID($ID);
        $ar_res = $res->fetch();
        if($iblock_id == IBLOCK_CAT_ID && $ar_res["SHOW_COUNTER"]>1)
        {
            $GLOBALS['DB']->RollBack();
            $el = new CIBlockElement;
            $ar = Array("ACTIVE" => "N");
            $el->Update($ID, $ar);
            global $APPLICATION;
            $APPLICATION->ThrowException("Товар деактивирован. Количество просмотров = ".$ar_res["SHOW_COUNTER"]);
            return false;
        }
    }
}

AddEventHandler("main", "OnAfterUserUpdate", Array("CMainHandler", "OnAfterUserUpdateHandler"));

class CMainHandler
{
    // пункт 3
    function OnAfterUserUpdateHandler(&$arFields)
    {
        $arUserGroups = CUser::GetUserGroup($arFields['ID']);
        $f=false;
        foreach ($arFields['GROUP_ID'] as $idGroup)
        {
            if(GROUP_CONTENT_ID == $idGroup['GROUP_ID'])
                $f=true;
        }
        if(!in_array(GROUP_CONTENT_ID,$arUserGroups) && $f)
        {
            $arFilter = Array("GROUPS_ID" => Array(GROUP_CONTENT_ID));
            $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
            $arEmail = array();
            while($arResUser = $rsUsers->GetNext())
            {
                $arEmail[] = $arResUser["EMAIL"];
            }
            if(count($arEmail) > 0)
            {
                $arEventFields = array(
                    "TEXT" => "Новый пользователь добавлен в вашу группу.",
                    "EMAIL" => implode(", ", $arEmail),
                    "NAME" => $arFields['NAME'],
                    "LAST_NAME" => $arFields['LAST_NAME'],
                    "NEW_USER_EMAIL" => $arFields['EMAIL'],
                );
                CEvent::Send("NEW_USER_CONTENT_GROUP", "s1", $arEventFields);
            }
        }
    }
}