<?
function AgentCheckActionDate()
{
    //echo "сработал";
    if(CModule::IncludeModule("iblock"))
    {
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_TO", "IBLOCK_ID");
        $arFilter = Array("IBLOCK_ID" => IBLOCK_ACTIONS_ID, "<DATE_ACTIVE_TO" => ConvertTimeStamp());
        $rsResCat = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $arItems = array();
        while ($arItemCat = $rsResCat->GetNext()) {
            $arItems[] = $arItemCat;
        }
        //test_dump($arItems);

        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => "CHECK_ACTION_DATE",
            "MODULE_ID" => "iblock",
            "ITEM_ID" => "",
            "DESCRIPTION" => "Проверка наличия акций с истекшей датой активности. Дата истекла для ".count($arItems)." акций",
        ));

        if (count($arItems) > 0) {
            $arFilter = Array(
                "GROUPS_ID" => Array(ADMIN_GROUP_ID)
            );
            $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $arFilter);
            $arEmail = array();
            while ($arResUser = $rsUsers->GetNext()) {
                $arEmail[] = $arResUser["EMAIL"];

            }
            //test_dump($arEmail);

            if (count($arEmail) > 0) {
                $arEventFields = array(
                    "TEXT" => count($arItems),
                    "EMAIL" => implode(", ", $arEmail),
                );
                CEvent::Send("CHECK_ACTIONS", "s1", $arEventFields);
            }
        }
    }
    return "AgentCheckActionDate();";
}
?>