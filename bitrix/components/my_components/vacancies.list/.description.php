<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_DESC_VACANCIES_LIST"),
	"DESCRIPTION" => GetMessage("IBLOCK_DESC_VACANCIES_DESC"),
	"PATH" => array(
		"ID" => "extra",
		"NAME" => GetMessage("IBLOCK_DESC_TREE_BRANCH_NAME"),
		"CHILD" => array(
			"ID" => "vacancies_ext",
			"NAME" => GetMessage("IBLOCK_DESC_VACANCIES"),
		)
	),
);

?>