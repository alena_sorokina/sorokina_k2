<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*************************************************************************
	Processing of received parameters
*************************************************************************/
//сбор и обработка параметров
if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 180;

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
$arParams['IBLOCKS_PROP'] = intval($arParams['IBLOCKS_PROP']);

//начало кеширования: $this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()))
if($arParams['IBLOCK_ID'] > 0 && $arParams['IBLOCKS_PROP'] > 0 && $this->StartResultCache(false, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups())))
{
	if(!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	// ПОЛУЧЕНИЕ ЭЛЕМЕНТОВ ИНФОБЛОКА
	//SELECT
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"IBLOCK_SECTION_ID",
		"NAME",
		"DETAIL_PAGE_URL",
	);
	//WHERE
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE"=>"Y",
		"CHECK_PERMISSIONS"=>"Y",
	);
	if($arParams["PARENT_SECTION"]>0)
	{
		$arFilter["SECTION_ID"] = $arParams["PARENT_SECTION"];
		$arFilter["INCLUDE_SUBSECTIONS"] = "Y";
	}
	//ORDER BY
	$arSort = array();
	//EXECUTE
	$rsIBlockElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);

	$f = false; //это чтобы, если GetList не вернул ничего, сделать $this->AbortResultCache();
	while($obElement = $rsIBlockElement->GetNextElement())
	{
		$f = true;
		$arItem = $obElement->GetFields();
		$arItem["PROP"] = $obElement->GetProperties();
		//формирование кнопок для поддержки технологии «Эрмитаж» для элементов
		$arButtons = CIBlock::GetPanelButtons(
			$arItem["IBLOCK_ID"],
			$arItem["ID"],
			$arItem["IBLOCK_SECTION_ID"],
			array("SECTION_BUTTONS"=>false, "SESSID"=>false)
		);
		$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

		$arResult["ITEMS"][] = $arItem;
		$arResult["IBLOCK_ID"]=$arItem["IBLOCK_ID"];
	}

	// ПОЛУЧЕНИЕ РАЗДЕЛОВ ИНФОБЛОКА
	$arSelect = array("ID", "IBLOCK_ID", "NAME",);
	//WHERE
	$arFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "CHECK_PERMISSIONS"=>"Y",);
	//ORDER BY
	$arSort = array();
	$rsIBlockSection = CIBlockSection::GetList($arSort, $arFilter, true, $arSelect, false);
	while($obElement = $rsIBlockSection->GetNext())
	{
		$f = true;
		$arResult["SECTIONS"][] = $obElement;
	}

	if($f)
	{
		//формирование массива с ключами кеша
		$this->SetResultCacheKeys(array());
		//подключение шаблона компонента
		$this->IncludeComponentTemplate();
	}
	else
	{
		$this->AbortResultCache();
	}
}

if(
	$arResult["IBLOCK_ID"] > 0
	&& $USER->IsAuthorized()
	&& $APPLICATION->GetShowIncludeAreas()
	&& CModule::IncludeModule("iblock")
)
{
	$arButtons = CIBlock::GetPanelButtons($arResult["IBLOCK_ID"], 0, 0, array("SECTION_BUTTONS"=>false));
	$this->addIncludeAreaIcons(CIBlock::GetComponentMenu($APPLICATION->GetPublicShowMode(), $arButtons));
}
?>
