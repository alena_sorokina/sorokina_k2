<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_VAC_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_VAC_DESCRIPTION"),
	"ICON" => "/images/aphisa.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "extra",
		"CHILD" => array(
			"ID" => "vacancies_ext",
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "vacancies_cmpx",
			),
		),
	),
);

?>