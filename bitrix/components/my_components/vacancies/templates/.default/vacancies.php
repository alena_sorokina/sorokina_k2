<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$APPLICATION->IncludeComponent(
    "my_components:vacancies.list",
    ".default",
    array(
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "IBLOCKS_PROP" => array(
            0 => "11",
            1 => "12",
            2 => "13",
        ),
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "COMPONENT_TEMPLATE" => ".default"
    ),
    $component
);?>
