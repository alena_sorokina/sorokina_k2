<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<a name="top"></a>

<style>
    .hide-item {
        display: none;
    }
</style>

<ul>
    <?foreach ($arResult['SECTIONS'] as $key => $arItem):?>
        <ul>
            <li class="<?="link-sect".$key?>"><a href=""><?=$arItem['NAME']?></a></li>
            <script>
                $('<?=".link-sect".$key?>').click(function(){
                    $('<?=".show-item-sect".$key?>').toggle();
                    return false;
                });
            </script>
            <?foreach ($arResult['ITEMS'] as $val):?>
                <ul >
                    <?if($arItem['ID'] == $val["IBLOCK_SECTION_ID"]):?>
                        <?
                        $this->AddEditAction($val['ID'], $val['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($val['ID'], $val['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>

                        <li id="<?=$this->GetEditAreaId($val['ID']);?>" class = "hide-item <?="show-item-sect".$key?>"><a href="<?=$val["DETAIL_PAGE_URL"]?>"><?=$val['NAME']?></a></li>
                    <?endif;?>
                </ul>
            <?endforeach;?>
        </ul>
    <?endforeach;?>
</ul>




