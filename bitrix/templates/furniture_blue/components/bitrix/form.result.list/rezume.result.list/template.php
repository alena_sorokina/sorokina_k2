<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//echo "<pre>arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>arResult: "; print_r($arResult); echo "</pre>";
//echo "<pre>"; print_r($arResult["arrFORM_FILTER"]); echo "</pre>";
?>

	<table class="form-table data-table">
		<?
		/***********************************************
					  table header
		************************************************/
		?>
		<thead>
			<tr>
				<th>
					<table class="form-results-header-inline">
						<tr>
							<th>
							ID<?if ($arParams["SHOW_STATUS"]=="Y") { ?><br /><?=SortingEx("s_id")?><? } //endif($SHOW_STATUS!="Y");?></th>
						</tr>
					</table>
				</th>
				<th><?=GetMessage("FORM_TIMESTAMP")?><br /><?=SortingEx("s_timestamp")?></th>
				<?
				if ($arParams["F_RIGHT"] >= 25)
				{
				?>
				<th>
					<table class="form-results-header-inline">
						<?
						if ($arParams["isStatisticIncluded"])
						{
						?>
						<tr>
							<th><?=GetMessage("FORM_USER")?></th>
							<td><?=SortingEx("s_user_id")?></td>
						</tr>
						<tr>
							<th><?=GetMessage("FORM_GUEST_ID")?></th>
							<td><?=SortingEx("s_guest_id")?></td>
						</tr>
						<tr>
							<th><?=GetMessage("FORM_SESSION_ID")?></th>
							<td><?=SortingEx("s_session_id")?></td>
						</tr>
						<?
						}
						else
						{?>
						<tr>
							<td><?=GetMessage("FORM_USER")?></td>
						</tr>
						<tr>
							<td><?=SortingEx("s_user_id")?></td>
						</tr>
						<?
						} //endif(isStatisticIncluded);
						?>
					</table>
				</th>
				<?
				} //endif;($F_RIGHT>=25)
				?>
				<?
				$colspan = 4;
				if (is_array($arResult["arrColumns"]))
				{
					foreach ($arResult["arrColumns"] as $arrCol)
					{
						if (!is_array($arParams["arrNOT_SHOW_TABLE"]) || !in_array($arrCol["SID"], $arParams["arrNOT_SHOW_TABLE"]))
						{
							if (($arrCol["ADDITIONAL"]=="Y" && $arParams["SHOW_ADDITIONAL"]=="Y") || $arrCol["ADDITIONAL"]!="Y")
							{
								$colspan++;
								?>
				<th>
								<?
								if ($arParams["F_RIGHT"] >= 25)
								{
								?>
					[<a title="<?=GetMessage("FORM_FIELD_PARAMS")?>" href="/bitrix/admin/form_field_edit.php?lang=<?=LANGUAGE_ID?>&ID=<?=$arrCol["ID"]?>&FORM_ID=<?=$arParams["WEB_FORM_ID"]?>&WEB_FORM_ID=<?=$arParams["WEB_FORM_ID"]?>&additional=<?=$arrCol["ADDITIONAL"]?>"><?=$arrCol["ID"]?></a>]<br /><?
								}//endif($F_RIGHT>=25);
								?>
								<?=$arrCol["RESULTS_TABLE_TITLE"]?>
				</th><?
							} //endif(($arrCol["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrCol["ADDITIONAL"]!="Y");
						} //endif(!is_array($arrNOT_SHOW_TABLE) || !in_array($arrCol["SID"],$arrNOT_SHOW_TABLE));
					} //foreach
				} //endif(is_array($arrColumns)) ;
				?>
			</tr>
		</thead>
		<?
		/***********************************************
					  table body
		************************************************/
		?>
		<?
		if(count($arResult["arrResults"]) > 0)
		{
			?>
			<tbody>
			<?
			//$j=0;
			foreach ($arResult["arrResults"] as $arRes) {
                if ($arParams["TODAY"] == "Y" && date("d.m.Y", strtotime($arRes["DATE_CREATE"])) == date("d.m.Y") || $arParams["TODAY"] == "N") {
                /*    $j++;

                    if ($arParams["SHOW_STATUS"] == "Y" || $arParams["can_delete_some"] && $arRes["can_delete"]) {
                        if ($j > 1) {
                            ?>
                            <tr>
                                <td colspan="<?= $colspan ?>" class="form-results-delimiter">&nbsp;</td>
                            </tr>
                            <?
                        } //endif ($j>1);
                        ?>

                        <?
                    } //endif ($SHOW_STATUS=="Y");
                */
                    ?>

                    <tr>
                        <td>
                            <b><?=$arRes["ID"]?></b><br/>
                        </td>
                        <td><?= $arRes["TSX_0"] ?><br/><?= $arRes["TSX_1"] ?></td>
                        <?
                        if ($arParams["F_RIGHT"] >= 25) {
                            ?>
                            <td><?
                                if ($arRes["USER_ID"] > 0) {
                                    $userName = array("NAME" => $arRes["USER_FIRST_NAME"], "LAST_NAME" => $arRes["USER_LAST_NAME"], "SECOND_NAME" => $arRes["USER_SECOND_NAME"], "LOGIN" => $arRes["LOGIN"]);
                                    ?>
                                    [<a title="<?= GetMessage("FORM_EDIT_USER") ?>"
                                        href="/bitrix/admin/user_edit.php?lang=<?= LANGUAGE_ID ?>&ID=<?= $arRes["USER_ID"] ?>"><?= $arRes["USER_ID"] ?></a>] (<?= $arRes["LOGIN"] ?>) <?= CUser::FormatName($arParams["NAME_TEMPLATE"], $userName) ?>
                                    <?
                                    if ($arRes["USER_AUTH"] == "N") { ?><?= GetMessage("FORM_NOT_AUTH") ?><?
                                    } ?>
                                    <?
                                } else {
                                    ?>
                                    <?= GetMessage("FORM_NOT_REGISTERED") ?>
                                    <?
                                } // endif ($GLOBALS["f_USER_ID"]>0);
                                ?>
                                <?
                                if ($arParams["isStatisticIncluded"]) {
                                    if (intval($arRes["STAT_GUEST_ID"]) > 0) {
                                        ?>
                                        [<a title="<?= GetMessage("FORM_GUEST") ?>"
                                            href="/bitrix/admin/guest_list.php?lang=<?= LANGUAGE_ID ?>&find_id=<?= $arRes["STAT_GUEST_ID"] ?>&set_filter=Y"><?= $arRes["STAT_GUEST_ID"] ?></a>]
                                        <?
                                    } //endif ((intval($GLOBALS["f_STAT_GUEST_ID"])>0));
                                    ?>
                                    <?
                                    if (intval($arRes["STAT_SESSION_ID"]) > 0) {
                                        ?>
                                        (<a title="<?= GetMessage("FORM_SESSION") ?>"
                                            href="/bitrix/admin/session_list.php?lang=<?= LANGUAGE_ID ?>&find_id=<?= $arRes["STAT_SESSION_ID"] ?>&set_filter=Y"><?= $arRes["STAT_SESSION_ID"] ?></a>)
                                        <?
                                    } //endif ((intval($GLOBALS["f_STAT_SESSION_ID"])>0));
                                } //endif (isStatisitcIncluded);
                                ?></td>
                            <?
                        } //endif ($F_RIGHT>=25);
                        ?>
                        <?
                        foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC) {
                            if (!is_array($arParams["arrNOT_SHOW_TABLE"]) || !in_array($arrC["SID"], $arParams["arrNOT_SHOW_TABLE"])) {
                                if (($arrC["ADDITIONAL"] == "Y" && $arParams["SHOW_ADDITIONAL"] == "Y") || $arrC["ADDITIONAL"] != "Y") {
                                    ?>
                                    <td>
                                        <?
                                        $arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
                                        if (is_array($arrAnswer)) {
                                            foreach ($arrAnswer as $key => $arrA) {
                                                ?>
                                                <? if (strlen(trim($arrA["USER_TEXT"])) > 0) { ?><?= $arrA["USER_TEXT"] ?>
                                                    <br/><? } ?>
                                                <? if (strlen(trim($arrA["ANSWER_TEXT"])) > 0) { ?>[<span
                                                        class='form-anstext'><?= $arrA["ANSWER_TEXT"] ?></span>]&nbsp;<? } ?>
                                                <? if (strlen(trim($arrA["ANSWER_VALUE"])) > 0 && $arParams["SHOW_ANSWER_VALUE"] == "Y") { ?>(
                                                    <span class='form-ansvalue'><?= $arrA["ANSWER_VALUE"] ?></span>)<? } ?>
                                                <br/>
                                                <?
                                                if (intval($arrA["USER_FILE_ID"]) > 0) {
                                                    if ($arrA["USER_FILE_IS_IMAGE"] == "Y") {
                                                        ?>
                                                        <?= $arrA["USER_FILE_IMAGE_CODE"] ?>
                                                        <?
                                                    } else {
                                                        ?>
                                                        <a title="<?= GetMessage("FORM_VIEW_FILE") ?>" target="_blank"
                                                           href="/bitrix/tools/form_show_file.php?rid=<?= $arRes["ID"] ?>&hash=<?= $arrA["USER_FILE_HASH"] ?>&lang=<?= LANGUAGE_ID ?>"><?= $arrA["USER_FILE_NAME"] ?></a>
                                                        <br/>
                                                        (<?= $arrA["USER_FILE_SIZE_TEXT"] ?>)<br/>
                                                        [&nbsp;<a
                                                                title="<?= str_replace("#FILE_NAME#", $arrA["USER_FILE_NAME"], GetMessage("FORM_DOWNLOAD_FILE")) ?>"
                                                                href="/bitrix/tools/form_show_file.php?rid=<?= $arRes["ID"] ?>&hash=<?= $arrA["USER_FILE_HASH"] ?>&lang=<?= LANGUAGE_ID ?>&action=download"><?= GetMessage("FORM_DOWNLOAD") ?></a>&nbsp;]
                                                        <?
                                                    }
                                                }
                                                ?>
                                                <?
                                            } //foreach
                                        } // endif (is_array($arrAnswer));
                                        ?>
                                    </td>
                                    <?
                                } //endif (($arrC["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrC["ADDITIONAL"]!="Y") ;
                            } // endif (!is_array($arrNOT_SHOW_TABLE) || !in_array($arrC["SID"],$arrNOT_SHOW_TABLE));
                        } //foreach
                        ?>
                    </tr>
                    <?
                }
            }//foreach
			?>
			</tbody>
		<?
		}
		?>
	</table>

</form>