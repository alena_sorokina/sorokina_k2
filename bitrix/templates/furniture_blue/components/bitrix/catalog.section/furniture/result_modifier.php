<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

//информация про материал продуктов
$arSort = false;
$arFilter = array("IBLOCK_ID" => IBLOCK_CAT_ID, "ACTIVE" => "Y", "SECTION_ID" => $arResult['ORIGINAL_PARAMETERS']['SECTION_ID']);
$arGroupBy = array("PROPERTY_MATERIAL");
$BDRes = CIBlockElement::GetList($arSort, $arFilter, $arGroupBy);
$arResult["CAT_ELEM"] = array();
while($arRes = $BDRes->GetNext())
{
    $arResult["CAT_ELEM"][] = $arRes;
}

foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$arItem['PRICES']['PRICE']['PRINT_VALUE'] = number_format($arItem['PRICES']['PRICE']['PRINT_VALUE'], 0, '.', ' ');
	$arItem['PRICES']['PRICE']['PRINT_VALUE'] .= ' '.$arItem['PROPERTIES']['PRICECURRENCY']['VALUE_ENUM'];
	
	$arResult['ITEMS'][$key] = $arItem;
}
?>