<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
    $().ready(function(){
        $(function(){
            $('#slides').slides({
                preload: false,
                generateNextPrev: false,
                autoHeight: true,
                play: 4000,
                effect: 'fade'
            });
        });
    });
</script>
<div class="sl_slider" id="slides">
    <div class="slides_container">
        <?foreach($arResult["ITEMS"] as $ID=>$arItem):?>
            <div>
                <div>
                    <?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                        <a href="<?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]['LINK']['VALUE']]["DETAIL_PAGE_URL"]?>" title="<?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]['LINK']['VALUE']]["NAME"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["src"]?>" alt="" /></a>
                    <?endif;?>
                    <h2><?echo $arItem["NAME"]?></h2>
                    <p><?echo $arItem["PREVIEW_TEXT"];?></p>
                    <p><?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]['LINK']['VALUE']]["NAME"]?> всего за <?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]['LINK']['VALUE']]["PROPERTY_PRICE_VALUE"]?> руб.</p>
                    <a href="<?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]['LINK']['VALUE']]["DETAIL_PAGE_URL"]?>" title="<?=$arResult["CAT_ELEM"][$arItem["PROPERTIES"]['LINK']['VALUE']]["NAME"]?>" class="sl_more">Подробнее &rarr;</a>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>
