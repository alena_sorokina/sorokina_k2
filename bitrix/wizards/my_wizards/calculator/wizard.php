<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class SelectAction extends CWizardStep{
	
	// Инициализация
	function InitStep(){
		// ID шага
		$this->SetStepID("select_action");
		
		// Заголовок
		$this->SetTitle(GetMessage("SA_INIT_TITLE"));
		$this->SetSubTitle(GetMessage("SA_INIT_SUBTITLE"));
		
		// Навигация
		$this->SetNextStep("first_number");
	}

    function ShowStep()
    {
        $this->content .= "<table class='wizard-data-table'>";

        $this->content .= "<tr><th align = 'right'>" . GetMessage('SA_CHOOSE_ACTION') . "</th><td>";

        $this->content .= $this->ShowSelectField(
            "cur_action",
            Array(
                "addition" => "Сложение",
                "subtraction" => "Вычитание",
                "multiplication" => "Умножение",
                "division" => "Деление",
            )
        ) . "</td></tr>";

        $this->content .= "</table>";
    }
}

class FirstNumber extends CWizardStep{
	
	// Инициализация
	function InitStep(){
		// ID шага
		$this->SetStepID("first_number");
		
		// Заголовок
		$this->SetTitle(GetMessage("FN_INIT_TITLE"));
		$this->SetSubTitle(GetMessage("FN_INIT_SUBTITLE"));
		
		// Навигация
		$this->SetPrevStep("select_action");
        $this->SetNextStep("second_number");
	}
    function ShowStep()
    {
        $this->content .= GetMessage('FN_NUMBER')." ".$this->ShowInputField("text", "number1", Array("size" => 25)) ;
    }
    function OnPostForm(){
        $wizard =& $this->GetWizard();

        if($wizard->IsCancelButtonClick())
            return;

        $num1 = $wizard->GetVar("number1");

        if($num1 === '')
            $this->SetError(GetMessage('OPF_EMPTY_FIELD'));
        elseif(!is_numeric($num1))
            $this->SetError(GetMessage('OPF_NOT_NUMERIC'));
    }
}

class SecondNumber extends CWizardStep{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("second_number");

        // Заголовок
        $this->SetTitle(GetMessage("SN_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("SN_INIT_SUBTITLE"));

        // Навигация
        $this->SetPrevStep("first_number");
        $this->SetNextStep("show_expression");
    }

    function ShowStep()
    {
        $this->content .= GetMessage('SN_NUMBER')." ".$this->ShowInputField("text", "number2", Array("size" => 25)) ;
    }

    function OnPostForm(){
        $wizard =& $this->GetWizard();

        if($wizard->IsCancelButtonClick())
            return;

        $num2 = $wizard->GetVar("number2");

        if($num2 === '')
            $this->SetError(GetMessage('OPF_EMPTY_FIELD'));
        elseif(!is_numeric($num2))
            $this->SetError(GetMessage('OPF_NOT_NUMERIC'));
    }
}

class ShowExpression extends CWizardStep{

    // Инициализация
    function InitStep(){
        // ID шага
        $this->SetStepID("show_expression");

        // Заголовок
        $this->SetTitle(GetMessage("SE_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("SE_INIT_SUBTITLE"));

        // Навигация
        $this->SetPrevStep("second_number");
        $this->SetFinishStep("final_step");
        $this->SetFinishCaption(GetMessage("SE_INIT_FINISH_CAPTION"));
    }

    function ShowStep()
    {
        $wizard =& $this->GetWizard();
        $num1 = $wizard->GetVar("number1");
        $num2 = $wizard->GetVar("number2");
        $cur_action = $wizard->GetVar("cur_action");

        $expression = CalculatorWizardTools::GetFinalExpression($num1, $num2, $cur_action);
        $this->content .= GetMessage('SE_EXPRESSION')." <b>".$expression."</b>";

        if($cur_action == "division" && $num2 == '0')
            $this->content .= "<br><b>".GetMessage('DIVISION_BY_ZERO')."</b>";
    }

    function OnPostForm(){
        $wizard =& $this->GetWizard();

        if($wizard->IsCancelButtonClick())
            return;

        $num2 = $wizard->GetVar("number2");
        $cur_action = $wizard->GetVar("cur_action");

        if($cur_action == "division" && $num2 == '0' && $wizard->IsFinishButtonClick())
            $wizard->SetCurrentStep("second_number");
    }
}

class FinalStep extends CWizardStep{

	// Инициализация
	function InitStep(){
		// ID шага
		$this->SetStepID("final_step");
		
		// Заголовок
		$this->SetTitle(GetMessage("FS_INIT_TITLE"));
		$this->SetSubTitle(GetMessage("FS_INIT_SUBTITLE"));

		// Навигация
		$this->SetCancelStep("");
		$this->SetCancelCaption(GetMessage("FS_INIT_CANCEL_CAPTION"));
	}

	function ShowStep(){

        $wizard =& $this->GetWizard();
        $num1 = $wizard->GetVar("number1");
        $num2 = $wizard->GetVar("number2");
        $cur_action = $wizard->GetVar("cur_action");

        $answer = CalculatorWizardTools::CalculateResult($num1, $num2, $cur_action);
		$this->content .= GetMessage("FS_RESULT") . " <b>" . $answer . "</b><br/>";

		CalculatorWizardTools::EventLogAdd($answer);
        $this->content .= "<br>" . GetMessage("FS_EVENT_LOG_ADD");
	}
}

class CalculatorWizardTools{

    //Получение готового выражения
    public function GetFinalExpression($num1, $num2, $cur_action)
    {
        switch ($cur_action) {
            case "addition":
                $expression = $num1." + ".$num2;
                break;
            case "subtraction":
                $expression = $num1." - ".$num2;
                break;
            case "multiplication":
                $expression = $num1." x ".$num2;
                break;
            case "division":
                $expression = $num1." / ".$num2;
                break;
        }
        return $expression;
    }

    //Подсчёт ответа
    public function CalculateResult($num1, $num2, $cur_action)
    {
        switch ($cur_action) {
            case "addition":
                $answer = $num1 + $num2;
                break;
            case "subtraction":
                $answer = $num1 - $num2;
                break;
            case "multiplication":
                $answer = $num1 * $num2;
                break;
            case "division":
                $answer = $num1 / $num2;
                break;
        }
        return $answer;
    }

    //Запись в журнал событий
    public function EventLogAdd($answer)
    {
        CEventLog::Add(array(
            "SEVERITY" => "INFO",
            "AUDIT_TYPE_ID" => "CALCULATOR_WIZARD_WORKED",
            "DESCRIPTION" => "Отработал мастер 'Калькулятор'. Ответ: ".$answer,
        ));
    }
}
?>