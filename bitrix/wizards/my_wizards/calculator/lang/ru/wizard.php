<?
$MESS['SA_INIT_TITLE'] = "Выбор действия";
$MESS['SA_INIT_SUBTITLE'] = "Выберите действие из предложенного списка";
$MESS['FN_INIT_TITLE'] = "Первое число";
$MESS['FN_INIT_SUBTITLE'] = "Введите первое число";
$MESS['SN_INIT_TITLE'] = "Второе число";
$MESS['SN_INIT_SUBTITLE'] = "Введите второе число";
$MESS['SE_INIT_TITLE'] = "Пример";
$MESS['SE_INIT_SUBTITLE'] = "Получившийся пример";
$MESS['SE_INIT_FINISH_CAPTION'] = "Посчитать";
$MESS['FS_INIT_TITLE'] = "Итог";
$MESS['FS_INIT_SUBTITLE'] = "Работа мастера успешно завершена!";
$MESS['FS_INIT_CANCEL_CAPTION'] = "Готово";
$MESS['SA_CHOOSE_ACTION'] = "Выберите действие:";
$MESS['FN_NUMBER'] = "Число №1:";
$MESS['SN_NUMBER'] = "Число №2:";
$MESS['OPF_NOT_NUMERIC'] = "Введенное значение не является числом!";
$MESS['OPF_EMPTY_FIELD'] = "Заполните поле!";
$MESS['DIVISION_BY_ZERO'] = "Недопустимое действие: деление на ноль! Чтобы исправить - вернитесь на предыдущий шаг.";
$MESS['SE_EXPRESSION'] = "Пример к вычислению:";
$MESS['FS_RESULT'] = "Результат:";
$MESS['FS_EVENT_LOG_ADD'] = "Запись будет добавлена в журнал событий";
?>