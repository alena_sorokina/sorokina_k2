<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?

#
# INPUT PARAMS
#
$arGadgetParams["WEB_FORM_ID"] = intval($arGadgetParams["WEB_FORM_ID"]);
if ($arGadgetParams["WEB_FORM_ID"] <= 0)
   return false;

if ($arGadgetParams["TEMPLATE_URL"] == '') {
    $UrlTodayNo = '/bitrix/admin/form_result_list.php?lang=ru&WEB_FORM_ID='.$arGadgetParams["WEB_FORM_ID"]."&del_filter=Y";
    $UrlTodayYes = '/bitrix/admin/form_result_list.php?lang=ru&WEB_FORM_ID='.$arGadgetParams["WEB_FORM_ID"]."&set_filter=Y&adm_filter_applied=0&find_date_create_1_FILTER_PERIOD=day&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=".date("d.m.Y")."&find_date_create_2=".date("d.m.Y");
}
else {
    $arGadgetParams["TEMPLATE_URL"] = str_replace("#FORM_ID#", $arGadgetParams["WEB_FORM_ID"], $arGadgetParams["TEMPLATE_URL"]);
    $UrlTodayYes = str_replace("#CHECK#", "Y", $arGadgetParams["TEMPLATE_URL"]);
    $UrlTodayNo = str_replace("#CHECK#", "N", $arGadgetParams["TEMPLATE_URL"]);
}

//test_dump($arGadgetParams["TEMPLATE_URL"]);
#
# CACHE
#
$obCache = new CPageCache;
$cacheTime = 3600;
$cacheId = $arGadgetParams["WEB_FORM_ID"].$arGadgetParams["TEMPLATE_URL"];

if($obCache->StartDataCache($cacheTime, $cacheId, "/")):
	if(!CModule::IncludeModule("form"))
	{
		ShowError(GetMessage("FORM_MODULE_NOT_INSTALLED"));
		return;
	}
    $rsResults = CFormResult::GetList($arGadgetParams["WEB_FORM_ID"],($by = "s_id"),($order="desc"),
        array("TIMESTAMP_1" => date("d.m.Y")), $is_filtered, "Y", false);
    while ($ob = $rsResults->Fetch())
    {
        $arResult[$ob["ID"]] = $ob["ID"];
    }
    $allResults = CFormResult::GetCount($arGadgetParams["WEB_FORM_ID"]);
    ?>
    <div style="margin-bottom: 10px;">
        За сегодня: <a href="<?=$UrlTodayYes?>"><?=count($arResult)?></a><br/>
        За все время: <a href="<?=$UrlTodayNo?>"><?=$allResults?></a><br/>
	</div>
<?
	$obCache->EndDataCache();
endif;

?>