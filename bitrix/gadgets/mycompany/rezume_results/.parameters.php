<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//подключаем модуль веб-форм
if(!CModule::IncludeModule("form"))
	return false;

//получаем все веб-формы
$arForms = array("" => GetMessage("GD_REZUME_EMPTY"));
$rsForm = CForm::GetList($by="s_id", $order="desc", false, $is_filtered);
while ($arForm = $rsForm->Fetch())
{
    //ShowMessage(Array("TYPE"=>"OK", "MESSAGE"=>var_export($arForm)));
    $arForms[$arForm["ID"]] = "[".$arForm["ID"]."] ".$arForm["NAME"];
}

//test_dump($arForms);
$arParameters = Array(
	"PARAMETERS"=> Array(
		"WEB_FORM_ID" => Array(
			"NAME" => GetMessage("GD_WEB_FORM_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arForms,
			"MULTIPLE" => "N",
			"DEFAULT" => '',
			"REFRESH" => "Y",
		),
	),
	"USER_PARAMETERS"=> Array(
		"TEMPLATE_URL" => array(
			"NAME" => GetMessage("GD_REZUME_REZULT_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => 'result.php?FORM_ID=#FORM_ID#&today=#CHECK#',
		),
	),
);

?>