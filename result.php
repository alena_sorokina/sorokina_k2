<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Список резюме за сегодня");

if($_GET["today"] == "Y")
{
    $APPLICATION->SetTitle("За сегодня");
    $today = "Y";
}
else
{
    $APPLICATION->SetTitle("За все время");
    $today = "N";
}

?><?$APPLICATION->IncludeComponent(
	"bitrix:form.result.list", 
	"rezume.result.list", 
	array(
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"NEW_URL" => "result_new.php",
		"NOT_SHOW_FILTER" => "",
		"NOT_SHOW_TABLE" => "",
		"SEF_MODE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_STATUS" => "Y",
		"VIEW_URL" => "result_view.php",
		"WEB_FORM_ID" => $_GET["FORM_ID"],
		"TODAY" => $today,
		"COMPONENT_TEMPLATE" => "rezume.result.list"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>