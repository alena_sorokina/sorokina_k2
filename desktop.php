<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Рабочий стол");
$APPLICATION->SetTitle("Рабочий стол");
?><?$APPLICATION->IncludeComponent(
	"bitrix:desktop", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"ID" => "holder1s1",
		"CAN_EDIT" => "Y",
		"COLUMNS" => "3",
		"COLUMN_WIDTH_0" => "33%",
		"COLUMN_WIDTH_1" => "33%",
		"COLUMN_WIDTH_2" => "33%",
		"GADGETS" => array(
			0 => "REZUME_RESULTS",
		),
		"G_REZUME_RESULTS_WEB_FORM_ID" => "1",
		"GU_REZUME_RESULTS_TITLE_STD" => "",
		"GU_REZUME_RESULTS_TEMPLATE_URL" => "result.php?FORM_ID=#FORM_ID#&today=#CHECK#"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>